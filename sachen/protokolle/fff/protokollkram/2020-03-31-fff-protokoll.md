# 2020-03-31 Protokoll Plenum

Anwesende AK-Leitungen: Leon A., Lara, Janika, Elias, Levin
Anwesende Delis: Lucia, Luis
Abwesend: Philipp, Alex, Thomas
Moderation: Tom
Protokoll: Tom

[TOC]

## Anliegen der Gäste
Es gibt keine Angliegen von Gästen.

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Nichts neues.

### AK Demo (Leon A.)
* Nichts neues.

### AK Mobi (Lara)
* Morgen findet eine Telko statt. Ansonsten nichts neues.

### AK Aktionen (Janika)
* Es wurde eine Foto-Aktion geplant. 
* Es wird zusammen mit dem AKK eine Social-Media-Aktion geplant.

### AK Kommunikation (Elias)
* Gestern fand eine sehr gute Teleonkonferenz statt
* Die Social-Media-Kanäle werden deutlich gestärkt
* Es wird tägliche Posts geben, wobei jeder Wochentag ein bestimmtes Motto hat (Mehr dazu: Siehe TOP 2)
* Bitte alle dem CryptPad beitreten

### AK For Future (Alex)
* Nichts neues. Nebensächlich kann die Bauernhilfe-Aktion erwähnt werden.

### TF Strategie (Levin)
* Telko fiel aus, daher derzeit nichts neues.

### Deliberichte
* Nichts neben den TOPs zu berichten, die es sowieso schon gibt.

## Abarbeitung der TOPs

### TOP 0: Abstimmungen
Beschreibung: Wir haben jetzt gerade eine verrückte Situation und haben im letzten Plenum beschlossen, dass wir Abstimmungen regeln möchten. Fun fact: Wir können über verschiedene Möglichkeiten abstimmen.

**Verfahrensvorschlag**
* Alle Teilnehmer:innen der Plenums-Telefonkonferenz, die nach dem Strukturpapier abstimmungsberechtigt sind, können abstimmen.
* Primäres Abstimmungsverfahren ist die FFF Plenum-Telegram-Gruppe.
* Falls jemand nicht auf Telegram zugreifen kann, aber an der Plenums-Telefonkonferenz teilnimmt, kann die Stimme manuell dazugerechnet werden.
* Die Ergebnisse werden nach der Abstimmung im Protokoll festgehalten. Spätere Stimmen werden nicht mehr berücksichtigt.
* Die Abstimmung erfolgt namentlich, nicht anonym, außer das Plenum entscheidet sich explizit dafür die Abstimmung anonym durchzuführen.
* Alle Abstimmungsfragen werden nummeriert, nur die Nummer der Abstimmungsfrage wird in den Fragetext der Telegram-Abstimmung übernommen.
  * Dabei entspricht die Nummer der Nummer des TOPs, bei mehreren Abstimmungsfragen zu einem TOP wird das Format "0.1", "0.2" usw. verwendet.
* Im Regelfall sind die Abstimmungsoptionen "Dafür", "Dagegen" und "Enthaltung"

#### Abstimmungsfrage 0
Wir möchten ab sofort mit Hilfe dieses Verfahrens in Telefonkonferenzen abstimmen. Diese Abstimmung wurde legitimerweise bereits nach diesem Verfahren durchgeführt.

**Ergebnis**: Dafür: 11, Dagegen: 1, Enthaltung: 1

### TOP 1: Offener Brief von der LA21A
Frist: 31.03.2020

Beschreibung: Die Lokale Agenda 21 Augsburg hat einen offenen Brief zum Thema Geflüchtete auf Lesbos verfasst. Sie bittet die Lokalpolitiker\*innen in Augsburg Platz zu schaffen, um wenigstens einen Teil der Kinder und Jugendlichen in Lebensgefahr hier in Augsburg aufzunehmen. (Genauer Brief und mehr im Plenumschat)

#### Abstimmungsfrage 1.1
Dazu wollen wir im Rahmen der Mottotage auf den sozialen Medien einen Beitrag veröffentlichen, der erklärt wie die Themen Klimapolitik und Geflüchtetenpolitk zusammenhängen. Falls Abstimmungsfrage 1.3 abgelehnt wird, ist dieses Abstimmungsergebnis hinfällig.

**Ergebnis**: Dafür: 8, Dagegen: 1, Enthaltung 4
#### Abstimmungsfrage 1.2
Dazu wollen wir eine Pressemitteilung veröffentlichen, die erklärt wie die Themen Klimapolitik und Geflüchtetenpolitk zusammenhängen. Falls Abstimmungsfrage 1.3 abgelehnt wird, ist dieses Abstimmungsergebnis hinfällig.

**Ergebnis**: Dafür: 1, Dagegen: 8, Enthaltung 4
#### Abstimmungsfrage 1.3
Wollen wir als FFF Augsburg den offenen Brief der Lokalen Agenda 21 Augsburg zum Thema Geflüchtete auf Lesbos mitunterzeichen? 

**Ergebnis**: Dafür: 10, Dagegen: 0, 3 Enthaltung 

### TOP 2: Mottotage Ideensammlung
Frist: 31.03.2020

Beschreibung: Ihr bekommt Zugang zu einem Pad in dem ihr Ideen für unsere ab jetzt täglichen Posts auf den sozialen Medien einreichen könnt. Die Mottotage sind (Änderungen vorbehalten)

* Mitmachen-Montag
* Divestment-Dienstag
* Meme-Mittwoch
* Durchstarten-Donnerstag
* Fakten-Freitag (mit Online-Streik) 
* Skandal-Samstag
* Sonniger-Sonntag

Abstimmungsfragen: 
Liegt im Entscheidungsbereich des AKK. Der TOP dient nur zur Information und zum Aufruf zur Teilnahme.

### TOP 3: Deli Finanzabstimmung Web AG
Frist: 07.04.2020

Beschreibung: Liebe Leute, 🔥🥳

die Web AG will die FridaysForFuture Cloud ausbauen und Website verbessern! ☁🖥

Der Ausbau der Cloud ist natürlich mit einigen Ausgaben verbunden, die aber durch gute Planung und viel ehrenamtliche Arbeit recht niedrig gehalten werden konnte. Um die über 50 Ortsgruppen die schon die Cloud verwenden dies weiter zu ermöglichen und neuen den Einsteig zu ermöglichen, brauchen wir eure Unterstützung ✊🏽

Wir, die Web AG, brauchen eure finanzielle Unterstützung. Daher beantragen wir vom Bundeskonto folgende Mittel:

Der von uns beantragte Finanzrahmen beläuft sich inklusive ausreichender Puffer auf insgesamt 5.500 Euro. Für so eine IT Infrastruktur ist das sehr cheap! Attack zahlt im Jahr fasst 150.000€ nur für Server!
Eine detaillierte Kostenaufstellung findet ihr in der Abstimmung.

Sämtliche Zahlen sind als Maximalwert gedacht. Alles was nicht verwendet wird, bleibt selbstverständlich auf dem Bundeskonto.

Die Abstimmung läuft über 11 Tage bis zum 08.04.

Dieser Ausbau wird krass und wir freuen uns über euren Support! 🌻💚

#### Abstimmungsfrage 3 
Legen wir ein Veto gegen den Finanzantrag der Webseiten-AG (Bundesebene) ein? Tom liefert die genaue Vetobegründung nach.

**Ergebnis:** Für das Veto: 10, Gegen das Veto 0, Enthaltung 2

### TOP 4: Deli Abstimmung: Legitimierung der Awareness-/Diversity-AG
Frist: 09.04.2020

Beschreibung: 🌱 *Information aus der CTF* 🌱
_—> zur Abstimmung über die AG-Legitimierung der Awareness- / Diversity-AG_

ℹ️ Nachdem sich die Awareness- / Diversity-AG am 29.03.2020 in der Deli-TK vorgestellt hat, folgt hier nun die reguläre Abstimmung zur Legitimierung der AG.

▶️ Bitte stimmt bis zum *09.04.2020, 21:00 Uhr* ab.

👉 Hier die Kompetenzen der Awareness- / Diversity-AG: https://fffutu.re/awarenessdiversity

Liebe Grüße
Eure CTF 💚

#### Abstimmungsfrage 4
Sind wir dafür, dass die Awareness-/Diversity-AG bundesweit legitimiert wird?

**Ergebnis:** Dafür: 13, Dagegen: 0, Enthaltung: 0

### TOP 5: Zukunftspreis 2020
Frist: 30.04.2020

Beschreibung: Wir haben uns letztes Jahr für den Zukunftspreis Augsburg beworben. Gewinn sind 1000 €. Dieses Jahr wieder?

#### Abstimmungsfrage 5
Sollen wir uns für den Zukunftspreis 2020 bewerben?

**Ergebnis:** Dafür: 12, Dagegen: 0, Enthaltung: 0

### TOP 6: Inaktive Mitglieder
Es gibt keine Frist.

Beschreibung: Ein kurzes Update zum Status der inaktiven Mitglieder und wie wir damit umgehen.

### TOP 7: Rede Ostermarsch
Da die Person, die den TOP eingereicht hat, nicht answesend ist und nicht erreicht werden konnte, wird der TOP verschoben.

### TOP 8: Werbung Foto-Aktion
Es gibt keine Frist.

Beschreibung: Beteiligt euch gerne, Details hier: (Link für das öffentliche Protokoll entfernt). Bei Fragen gerne Janika fragen. 

Es wurden keine Abstimmungsfragen eingereicht.



## Sonstiges

Das Protokoll wurde verlesen: Nein, da alle Zugriff auf das Protokoll hatten, während es erstellt wird.

Das Plenum endete um 21:35 Uhr