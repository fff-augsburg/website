# 2020-06-16 Protokoll Plenum

* Anwesende AK-Leitungen: Alex, Lara, Janika, Leon, Elias
* Anwesende Delis: Luis, Lucia
* Abwesend: Philipp, Thomas
* Moderation: Tom
* Protokoll: Alex, Tom

[TOC]

## Anliegen der Gäste
Heute keine Gäste.

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Nichts

### AK Demo (Leon A.)
* Nichts

### AK Mobi (Lara)
* Nichts

### AK Aktionen (Janika)
* Platzpark in Planung.

### AK Kommunikation (Elias)
* Nichts

### AK For Future (Alex)
* Radentscheid: Treffen zwischen Radentscheidvertretern (Arne, Jens, Alex) und OB und paar anderen PolitikerInnen (Weber, von Mutius, Dietz, Merkle)

### TF Strategie (Levin)
* Nichts

### Deliberichte
* bundesweiter Aktionstag am 01.07. (Mi), wg EU-Ratspräsidentschaft ab 01.07., nur alle 13 Jahre und DE spielt eine Rolle. ist bisschen kompliziert, siehe 
  * https://de.wikipedia.org/wiki/Rat_der_Europ%C3%A4ischen_Union#Vorsitz und Infos folgen demnächst von Bundesebene

### Kafka-Berichte
* Nichts

## Abarbeitung der TOPs


### TOP 1: TF-Strategie Leitung

* Nominierungen
  * Leon Ü.: abgelehnt 
  * Isabell: Nicht anwesend
  * Robil: Er nimmt die Nominierung an
  * Luis: Er nimmt die Nominierung an
  * Elias: Er nimmt die Nominierung an

Elias und Luis stellen sich gemeinsam zur Wahl.

#### Abstimmungsfrage 1
Abstimmungsverfahren: Namentliche Abstimmung. Wenn im ersten Wahldurchgang kein Kandidat mehr als 50 % der abgegebenen Stimmen erhält (absolute Mehrheit), wird in der Reihenfolge der größten Zustimmung einzeln abgestimmt, bis ein Kandidat mindestens 50 % der Stimmen auf sich vereinen kann oder alle Wahlen scheitern. Im Falle des Scheiterns der Wahl tritt Levin zurück und der Posten bleibt unbesetzt.

Abstimmungsfrage: Wer soll den Posten "TF-Strategie-Leitung" für die nächsten 3 Monate besetzen?
* A: Robil
* B: Elias und Luis
* Enthaltung

**Ergebnis**: A: 3, B: 12, Enthaltung: 0



### TOP 2: Deli-Wahl

* Nominierungen
  * Noah: Er lehnt die Nominierung dankend ab
  * Leon A: Er lehnt die Nominierung dankend ab
* Angenommene Nominierungen
  * Robil: Er nimmt die Nominierung an
  * Luis: Er nimmt die Nominierung an
  * Lucia: Sie nimmt die Nominierung an
  * Sophia: Sie nimmt die Nominierung an
  * Levin: Er nimmt die Nominierung an
  * Janika: Sie nimmt die Nominierung an

#### Abstimmungsfrage 2.1
Abstimmung über das Abstimmungsverfahren: 
* Es werden die Delis für die Bundesebene gewählt, welche gleichzeitig unsere Delis auf Bayern-Ebene sind.
* Es wird digital in der internen Plenumsgruppe per Abstimmungsfunktion namentlich abgestimmt.
* Im ersten Wahldurchgang werden alle Kandidat:innen gleichzeitig zur Wahl gestellt. Alle haben dabei eine Stimme. Eine Enthaltung bedeutet, gegen alle möglichen Kandidat:innen zu stimmen.
* Wenn im ersten Wahldurchgang kein:e Kandidat:in mehr als 50 % der abgegebenen Stimmen erhält (absolute Mehrheit), wird in der Reihenfolge der größten Zustimmung einzeln abgestimmt, bis jeweils ein:e Kandidat:in mindestens 50 % der Stimmen auf sich vereinen kann oder alle Wahlen scheitern.
* Wenn bei der Abarbeitung der Liste mehrere Menschen gleich viele Stimmen auf sich vereinen, wird eine Stichwahl zwischen allen Kandidat:innen mit gleich vielen Stimmen durchgeführt
  * Wenn bei dieser Stichwahl ein:e Kandidat:in eine absolute Mehrheit auf sich vereinen kann, ist die Person direkt gewählt
  * Sollte keine Person bei der Stichwahl eine absolute Mehrheit auf sich vereinen, so legt die Anzahl der positiven Stimmen die Reihenfolge für einzelne Abstimmungen fest
  * Wenn mehrere Personen gleich viele Stimmen bekommen, werden diese Personen untereinander in einer weiteren Stichwahl gegeinenander aufgestellt, wobei die Reihenfolge insgesamt weiter nach der Anzahl der positiven Stimmen verläuft
* Es wird weiter abgestimmt, bis drei Personen gewählt wurden, die gemeinsam folgende Eigenschaften erfüllen
  * Es ist mindestens eine Person Weiblich (**F**emale), **I**ntersexuell, **N**ichtbinär, **T**rans- oder **A**gender oder **\*** (kurz: FINTA\*)
* Im Falle des Scheiterns der Wahl bleiben die bisherigen Delis im Amt.
* Alle Kandidat:innen erhalten 2:30 Minuten Vorstellungs- und Fragezeit (zusammengerechnet), welche sie sich frei einteilen können. Die Zeit in der Fragen von anderen Menschen gestellt werden, zählt nicht dazu - nur die Antwortzeit.
* Nachdem alle Vorstellungen abgeschlossen sind wird das Wahlverfahren gestartet.
* Die Amtszeit beträgt drei Monate.

* **Ergebnis**: Dafür: 11, Dagegen: 0, Enthaltung: 2

#### Abstimmungsfrage 2.2.1
Wahldurchgang 1 mit allen Kandidat:innen. Wer eine absolute Mehrheit der Stimmen auf sich vereinen kann, ist direkt gewählt.

* **Ergebnis**:
  * Lucia: 5
  * Janika: 3
  * Robil und Sophia: 2
  * Luis und Levin: 1
  * Enthaltungen: 0
  
#### Abstimmungsfrage 2.2.2
Wahldurchgang 2. Soll Lucia Deli für die Bundesebene werden?

* **Ergebnis**: Dafür: 12, Dagegen: 0, Enthaltung: 0

#### Abstimmungsfrage 2.2.3
Wahldurchgang 3. Soll Janika Deli für die Bundesebene werden?

* **Ergebnis**: Dafür: 14, Dagegen: 0, Enthaltung: 0

#### Abstimmungsfrage 2.2.4
Stichwahl Robil und Sophia, Wahldurchgang 4. Soll Robil oder soll Sophia Deli für die Bundesebene werden?

**Ergebnis**: Robil: 5, Sophia: 9, Enthaltung: 0

#### Ergebnisse insgesamt
Damit sind Lucia, Janika und Sophia als Delis für die Bundesebene und für die Bayernebene für die nächsten drei Monate gewählt.  Janika bleibt bis zur Wahl einer neuen AK-Aktion-Leitung übergangsweise AK-Leitung des AK-Aktion. Es wird nächstes Plenum eine AK-Aktion-Leitungswahl durchgeführt. 



### TOP 3: Stadtradeln

Wir würden gerne gemeinsam mit dem Augsburger Klimarat eine übergreifende Gruppe für ganz Augsburg einrichten und gemeinsam als Klimagerechtigkeitsbewegung und mit Freund:innen und Sympathisant:innen Kilometer sammeln. Verantwortlich für die Durchführung ist der AK-Aktionen. Strategische Verbindungen mit dem Radentscheid und Fahrraddemos werden angestrebt. Falls möglich, wünschen wir uns, dass sich der Augsburger Klimarat gemeinsam hinter den Radentscheid stellt. Vor der Durchführung wird daher mit dem Radentscheid-Team und dem Augsburger Klimarat koodiniert.

### Neue Struktur für den AK-Kommunikation (AKK)

Da der AKK in seiner Struktur besonders ist und eine zentrale Leitungsposition nicht länger als sinnvoll betrachtet wird, wird die Position der AKK-Leitung von Elias und Leon (und anderen, die mitmachen möchten) überarbeitet und das Ergebnis nächstes Plenum zur Wahl gestellt.

## Sonstiges
* INTERN Physisches Plenum wird nächste Woche besprochen
* INTERN Abschied Sarah :(

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 23:40 Uhr