# 2020-06-09 Protokoll Plenum

* Anwesende AK-Leitungen: Janika, Leon A, Elias, Alex
* Anwesende Delis: Lucia
* Abwesend:
* Moderation: Tom
* Protokoll: Ingo

[TOC]

## Anliegen der Gäste

Keine Gäste anwesend.

## Berichte der AKs und TFs

### AK Struktur (Philipp), AK Demo (Leon A.), AK Mobi (Lara), Aktionen (Janika)
nichts neues

### AK Kommunikation (Elias)
Unser Post zu unserer Demo am 2.6.2020 gehört zu unseren meistgelikedtesten Posts überhaupt.

### AK For Future (Alex)

* Durch die Verteilaktion in Göggingen kommen viele Unterschriften für den Radentscheid rein.
* Demo für Grünabbiegepfeile in Planung.
* Es gab einen weiteren Klimaleugnungsvortrag, den wir gemeinschaftlich durcharbeiten können.
* Alex und die anderen offiziellen Vertreter\*innen vom Radentscheid treffen sich demnächst mit der Oberbürgermeisterin.

### TF Strategie (Levin)

Nichts Neues.

### Deliberichte

### Kafka-Berichte

* Drei Anträge werden in der Kafka "Strukturänderungen an der Bundesstruktur" besprochen: Abstimmung zum GÜAIG (Gremium-Übergriffs-Interventions-Arbeits-Gruppe) Legitimierungsantrag, Back-to-the-roots-Antrag, Abstimmung über neues Finanzkonzept
* Nichts Neues in der Kafka "Aktionskonsens und Selbstverständnis bundesweit"


## Abarbeitung der TOPs
* paar interne TOPs

## Sonstiges
nichts


## Mails 

"Liebe Scientists in Bayern,

Ich frage euch an, da wir euch gerne als Erstunterzeichner*innen sowie
Expert*innen für eine bayernweite Petition gewinnen wollen. Angesichts der
vielen sozialen wie ökologischen Krisen unserer Zeit haben das Landeskomitee
der Katholiken, der BUND Bayern in Person von Richard Mergner sowie die
Fridays for Future Ortsgruppe Nürnberg den beiliegenden „Bayernplan für eine
sozial-ökologische Transformation“ verfasst. Wir planen, diesen Aufruf am
19.06. öffentlich zu machen, einen Offenen Brief an Landtagspräsidentin Ilse
Aigner und Ministerpräsident Markus Söder zu richten und eine Petition zu
starten.

Bei dem Papier handelt es sich um einen Konsenstext, der die groben Züge einer
sozial-ökologischen Transformation beschreibt. Konkretere Vorschläge sollen
erst bei der Anhörung im Landtag vorgestellt werden. Die regionalgruppen der
Scientists, aber auch euch als Einzelpersonen wollen wir als Erstunterzeichner
beziehungsweise Expert*innen gewinnen. Wir würden uns sehr freuen, wenn ihr
uns in eurem jeweiligen Fachgebiet unterstützt.

Der Konsenstext selber ist im angehängten Dokument enthalten. Es besteht aus
vier Teilen:
1. Petitionstext ohne Quellen
2. Petitionstext mit Quellen
3. Themengebiete sowie Expert*innen für eine mögliche Anhörung im Landtag
4. Organisatorisches der Petition/ Zeitplan.

Solltet ihr die Petition als Erstunterzeichnerin unterstützen wollen, füllt
bitte das Formular auf der letzten Seite des angehängten Dokumentes aus und
schickt es per Fax an Jörg Alt, oder einen Scan per E-Mail an uns. Die Frist
ist Montag, der 15. Juni. Sollten Sie mehr Zeit benötigen, um eine
Unterzeichnung zu beraten, könnt ihr auch später nachziehen oder einfach als
einzelne Wissenschaftler*innen unterzeichnen. In diesem Formular befindet sich
auch ein Feld, in dem ihr eure jeweiligen Fachgebiete eintragen könnt. Falls
zu dieser doch sehr komplexen Angelegenheit fragen bestehen, könnt ihr euch
gerne jederzeit unter 01575 7609702 bei mir melden!

Ich freue mich über eine Rückmeldung per E-Mail, ob ihr euch unserer Sache
anschließt und uns inhaltlich unterstützt.

Mit vielen Grüßen aus Nürnberg,
Vincent Gewert
Im Namen der Fridays for Future Ortsgruppe Nürnberg"


Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 22:00 Uhr