# 2020-07-14 Protokoll Plenum

* Anwesende AK-Leitungen: Leon, 
* Anwesende Delis: Janika, Lucia, Sophia
* Abwesend: Janika, Phillip, Elias, Alex
* Moderation: Sophia
* Protokoll: Levin

[TOC]

## Anliegen der Gäste
nichts

## Berichte der AKs und TFs

### AK Struktur (Philipp)
wir brauchen eine neue Leitung

### AK Demo (Leon A.)
Überlegung für weitere Fahrraddemo

### AK Mobi (Lara), Aktionen (Janika), Kommunikation (Elias), For Future (Alex)
nichts

### TF Strategie (Elias)
nichts

### Deliberichte (Lucia, Janika, Sophia)
kommt später

### Kafka-Berichte
nichts

## Abarbeitung der TOPs




### Deli-Abstimmung: Termin Global-Streik

*Lucia // 29.06.2020, 13:16 Uhr*

**Frist:** 15.07.2020

**Beschreibung:**

>*🌎 NEXT GLOBAL CLIMATE STRIKE 🌏*
This is the poll for all LOCAL GROUPS to chose their preferred date for our next Global Day of Action. 💥 Please vote and share!
➡️ Each local Group has one vote
➡️ Deadline: 15 JULY 23:59 GMT (3 weeks from now!)
 ➡️ This is the poll: 
https://forms.gle/qckcBYf4bmpCh5d79
Thank You,
Your Date Planning Working Group 🥰

Wir besprechen das nicht, weil es uns ziemlich egal ist und niemand abstimmen möchte.



### AK Struktur Leitung Neuwahl

*Sophia // 06.07.2020, 15:54 Uhr*

Es gibt keine Frist.

**Beschreibung:**

>Philip war jetzt ja auch mal am Camp und hat bestätigt, dass er gern die Leitung für den AK Struktur abgeben würde, da er selbst nicht mehr sehr aktiv ist.

#### Abstimmungsfrage

>Wer könnte sich vorstellen die Leitung des AK Struktur zu übernehmen?

Levin meldet sich bereit zu kandidieren, sonst will leider niemand.
Soll Levin die neue Leitung des AK Struktur werden?

**Ergebnis:** 6 dafür, 0 dagegen, 1 Enthaltungen
 4 Leute sind im Plenum aber stimmen nicht ab.



## Sonstiges

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 20:10 Uhr