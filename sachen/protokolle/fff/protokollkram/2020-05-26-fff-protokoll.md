# 2020-05-26 Protokoll Plenum

* Anwesende AK-Leitungen: Janika, Elias, Leon, Lara
* Anwesende Delis: Lucia, Luis
* Abwesend:
* Moderation: Tom
* Protokoll: Ingo

[TOC]

## Anliegen der Gäste

Keine Gäste anwesend.

## Berichte der AKs und TFs

### AK Struktur (Philipp), Demo (Leon A.), Mobi (Lara), Aktionen (Janika)

Nichts neues.

### AK Kommunikation (Elias)

Nicht anwesend.

### AK For Future (Alex)

* Nächstes Vernetzungstreffen aller "muss handeln"-Initiativen diesen Donnerstag
* Radentscheid plant zahlreiche Aktionen
* Göggingen soll beflyert werden mit Unterschriftenlisten für den Radentscheid

### TF Strategie (Levin)

Nichts neues.

### Deliberichte

Nichts neues, was nicht ein eigenständiger TOP ist.

### Kafka-Berichte

## Abarbeitung der TOPs

### TOP 1: Demo am 2.6.

Am 2.6. sollen deutschlandweit FFF-Streiks stattfinden. Ein Brainstorming während des Plenums ergab folgende Ideen.

* In Zweiergruppen durch die Stadt ziehen (Bindfaden)
* Als 20er-Gruppe durch die Stadt ziehen (trotzdem je zwei Leute mit Seil+Plakate verbunden)
* Kreideaktion
* Lärmdemo
* Trommeln
* Geheim

Selbstverständlich werden wir unter Hinblick auf Corona auf alle Sicherheitsmaßnahmen achten.

Falls wir mitmachen, müssen wir noch entscheiden: wann genau? wo? und wer übernimmt die Versammlungsleitung?

#### Abstimmungsfrage 1
Wir schließen uns dem deutschlandweiten Streik am 2.6. an. Der AK-Demo soll dafür eine Demo anmelden, die einer Kombination der oben genannten Punkte entspricht.

**Ergebnis:** 11 dafür, 0 dagegen, 0 Enthaltungen

#### Abstimmungsfragen 1.1
Ingo übernimmt die Versammlungsleitung und meldet die Demo an.

**Ergebnis:** 8 dafür, 0 dagegen, 1 Enthaltungen

Nach Diskussion nach der Pause kristallisierte sich 11:00 Uhr als Termin heraus. Wir möchten nicht gleich zu Beginn der Schulöffnungen die Schule bestreiken, aber nächste Woche sind Ferien.

### TOP 2: Deli-Abstimmung zur Drei-Länder-Demo - „Unite for Climate“

Ortsgruppen von Fridays for Future Baden-Württemberg, Klimastreik Basel, sowie Youth for Climate Alsace wollen gemeinsam eine Drei-Länder-Demo im Frühjahr bzw. Frühsommer 2021 organisieren. Das ganze soll unter dem Motto „Unite for Climate“. Nun soll das Projekt von Fridays for Future Deutschland legitimiert werden.

#### Abstimmungsfrage 2

Wir legitimieren das Projekt "Drei-Länder-Demo". Bundesweite AGs dürfen das Projekt "Unite for Climate" unterstützen (z.B. bei der Mobi durch Posts auf SM). Das FfF-DE-Logo darf für "Unite for Climate" verwendet werden. 

**Ergebnis:** 11 dafür, 0 dagegen, 0 Enthaltungen

## Sonstiges

Es gibt keine Sonstigen TOPs.

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 22:00 Uhr.