# 2020-03-24 Protokoll Plenum

Anwesende AK-Leitungen: Philipp, Leon A., Lara, Janika, Levin, Elias(verspätet)

Anwesende Delis: Lucia, Luis

Abwesend: Alex, Thomas

Moderation: Tom

Protokoll: mehrere Leute, u.a. Tom und Ingo

[TOC]

## Anliegen der Gäste

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Strukturpapier überarbeiten / ausformulieren
* AKs "aufräumen" (lassen)
* Antworten der Stadt zu unseten Forderungen übersichtlich aufarbeiten und darauf reagieren, sobald der Stadtrat neu aufgestellt ist

### AK Demo (Leon A.)
* Wird die anderen AKs während der Corona-Zeit unterstützen, da sie selbst nichts durchführen können

### AK Mobi (Lara)
* Allgemeiner Flyer 
* Nachbesprechung der Mobi für den 1.3.
* Mobiplan für kommende große Demos/Aktionen ausarbeiten

### AK Aktionen (Janika)
* Es gab eine Telefonkonferenz
* Beteiligung an bundesweiten digitalen Aktionen
* Podiumsdiskussionen mit Live-Videos auf Instagram o.Ä.
* 

### AK Kommunikation (Elias)
* Nicht anwesend (Zitat: "Ich will auch keinen machen")

### AK For Future (Alex)
* Nichts zu berichten

### TF Strategie (Levin)
* Herzliche Einladung zu einer Telefonkonferenz jeden Donnerstag um 16:00 Uhr

### Deliberichte
* Wollen wir am 24.04. an einer bundesweiten Aktion teilnehmen? (Bundesweites Stimmungsbild)
  * Ergebnis: Nur an einer Online-Aktion
  * Abstimmungsfrage: Wollen wir den 24.04. als großen Aktionstag festhalten? Wir befürworten nur Online-Aktionen.
  * Dafür: 10, Dagegen: 1, Enthaltung: 2
* Alles weitere kommt im Plenumschat.


## Abarbeitung der TOPs

### TOP 1: Inaktive Mitglieder
* Es gibt inaktive Mitglieder. Diese sollen nach dem Verfahrn des Strukturpapiers informiert und ggf. entfernt werden.

Zitat aus dem Strukturpapier:
> **3.2 Aktive Mitglieder**
Wer sich als aktives Mitglied in einem AK qualifiziert, liegt im Ermessen der AK-Leitung.
Generell sollte als aktives AK-Mitglied jede Person zählen, die in diesem AK zugehörig sein
will und produktiv an den Aufgaben des AKs arbeitet.
Dies setzt vor allem Kooperation mit der AK-Leitung und anderen AK-Mitgliedern voraus.
Der/Die AK-Leiter*in soll eine Liste aller aktiven Mitglieder führen und aktuell halten, die
auch dem Struktur AK vorliegt.
Störende oder unbegründet inaktive Mitglieder sollen aus der Liste der aktiven Mitglieder
entfernt werden. Wenn nötig, auch aus den Strukturen des AKs (Chats, Gruppen o.ä.). Ein
Wiedereintritt soll jederzeit möglich sein, sobald man sich wieder für die AK-Leitung
ersichtlich produktiv einbringt. Vor dem Ausschluss aus dem AK muss diese Person über
das geplante Vorgehen informiert werden und die Chance erhalten, das Verhalten zu
ändern oder zu begründen.

### TOP 2: Bauer*innenhilfe
* Bauer*innen mangelt es an 300.000 Erntehelfer*innen, da wegen Corona Grenzen geschlossen sind
* Leon steht in Kontakt mit einem Familienbetrieb, der Spargel verkauft, sowie mit einem Bauernverbund. Die freuen sich über Hilfe; ohne Hilfe wird Nachrung ungeerntet verrotten
* Vorschlag: Wer möchte, fährt (individuell) hin und hilft
* Matthias von den Parents fährt diesen Freitag hin um alles mögliche zu besprechen
* Es gibt auch ein deutschlandweites Hilfevermittlungsportal. Gründe, nicht einfach dort mitzumachen, sind unter anderem: da wir so direkteren Kontakt zu den Bauer*innen bekommen; Matthias von den Parents schon viel dazu organisiert hat; wir in der Presse dann besser als Einzelinstitution wahrgenommen werden
* Tom: Erntehelfer*innen werden traditionell ausgebeutet! Sollten wir als Klimagerechtigkeitsbewegung so etwas unterstützen?
* Abstimmungsfrage 1: Sollen wir das deutschlandweite Jobportal für Enterhilfe offiziell über die Kanäle von FFF Aux bewerben? Dafür: 6, dagegen: 7, Enthaltung: 1
* Abstimmungsfrage 2a: Sollen wir die von Matthias organisierte Hilfsinitiative für bestimmte Familienbetriebe über die Kanäle von FFF Aux bewerben? (Die Pressemitteilung dazu soll im gleichen Atemzug auf die Missstände in der Landwirtschaft hinweisen, insbesondere auf die schlechten Arbeitsbedingungen.) Dafür: 10, dagegen: 3, Enthaltung: 0
  * Abstimmungsfrage 2b: Sollen wir das als Aktion von FFF Augsburg darstellen? Dafür: 7, dagegen: 5, Enthaltung: 0

## Sonstiges


Das Protokoll wurde verlesen: nein, aber es wurde kollaborativ über ein CryptPad erstellt

Das Plenum endete um 19:33 Uhr