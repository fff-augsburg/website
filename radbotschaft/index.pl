#!/usr/bin/env perl

use warnings;
use strict;

use CGI;
use Digest::SHA  qw< sha256_hex >;
use POSIX        qw< strftime >;
use Data::UUID;

sub escape_html {
    my $str = shift;
    $str =~ s/([<>"'&\0])/&#@{[ ord($1) ]};/g;
    return $str;
}

my @output;
my $changes;

my $q = CGI->new();

print $q->header(-type => "text/html; charset=UTF-8");

push @output, <<TMPL;
<!doctype html>
<html lang="de">
<head>
  <title>Radentscheid Augsburg – Radlbotschafter*innen</title>
  <meta name="viewport" content="initial-scale=1">
  <style>
    body { font-family: sans-serif; margin-left: auto; margin-right: auto; margin-top: 0; max-width: 55em; }
    .ambassador input { width: 100%; padding: 0.3em; }
    input { font-size: inherit; border: 1px solid #e7e7e7; border-radius: 2px; text-shadow: 0px 0px 5px #009ee0; }
    .bare { font-family: inherit; border: 0; background-color: white; text-shadow: 0px 0px 0px; color: black; font-weight: normal; }
    button { font-size: larger; border: 1px solid #e7e7e7; border-radius: 10px; text-shadow: 0px 0px 5px #009ee0; background: linear-gradient(to right, #009ee0 1%,#53b278 100%); color: white; font-weight: bold; }
    th { text-align: left; vertical-align: bottom; }
    tr td:nth-child(3) { text-align: right; }
    tr, p { padding: 0.3em; }
    h1 {
      text-align: center;
    }
    \@keyframes foo {
      0%   { background-color: #009ee0; }
      100% { background-color: white; }
    }
    .updated { animation: foo 1s ease-in-out; }
    .highlight {
      background: linear-gradient(to right, #009ee0 1%,#53b278 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
    p { text-align: justify; }
    table { border-collapse: collapse; }
    button { padding: 0px; }
    progress { width: 100%; }
  </style>
</head>
<body>

<script>
  function edit (id) {
    for(const s of ["name", "team", "count"]) {
      document.getElementById(s + "-" + id).contentEditable = true;
    }
    document.getElementById("edit-" + id).style.display = "none";
    document.getElementById("submit-" + id).style.display = "block";
    document.getElementById("name-" + id).focus();
  }

  function send (id) {
    location.replace(
      window.location.href.split(/[#?]/)[0] +
      "index.pl?id=" + id +
      "&name=" + encodeURIComponent(document.getElementById("name-" + id).innerText) +
      "&team=" + encodeURIComponent(document.getElementById("team-" + id).innerText) +
      "&count=" + encodeURIComponent(document.getElementById("count-" + id).innerText)
    );
    return false;
  }
</script>

<h1 class="highlight">Radlbotschafter*innen</h1>

<p>Der Augsburger Radentscheid hat <span class="highlight">gute Chancen</span>,
zum <span class="highlight">erfolgreichsten Bürgerbegehren</span> Augsburgs
aller Zeiten zu werden. So war es etwa auch in München. Dort halfen mehr als
Tausend Ratbotschafter*innen, in ihrem Umfeld Unterschriften zu sammeln.</p>

<p>Die Unterschriftenlisten werden pünktlich zur großen Fridays-for-Future-Demo am 1.
März angefertigt. Es können dann alle volljährigen EU-Bürger*innen unterschreiben, die
sich seit mindestens zwei Monaten in Augsburg mit dem Schwerpunkt ihrer
Lebensbeziehungen aufhalten.</p>
TMPL

my %ambassadors;

for my $file (glob "/home/iblech/radbotschaft/*.txt") {
    $file =~ /\/([\w\-]+)\.txt$/ or die;

    open my $fh, "<", $file or die $!;
    chomp(my $line = <$fh>);
    $ambassadors{$1} = [ split "\t", $line ];
}

my $new_entry = "";

if(defined($q->param("name"))) {
    my $name    = $q->param("name");
    my $team    = $q->param("team") // "";
    my $count   = $q->param("count");
    my $contact = $q->param("contact");
    my $address = $q->param("address") // "";
    my $id      = $q->param("id") // "";
    $id = Data::UUID->new()->create_str() unless $ambassadors{$id};

    my $date = strftime("%c", localtime);

    if(
	defined $name and defined $count and
	not ($name =~ /[\r\n]/ or $team =~ /[\r\n]/ or $count =~ /[^0-9]/) and
	length($name) and $count >= 0 and
	length $name < 100 and length $team < 100 and $count < 10000
    ) {
	open my $fh, ">", "/home/iblech/radbotschaft/$id.txt" or die $!;
	print $fh "$name\t$team\t$count\n";
	$ambassadors{$id} = [ $name, $team, $count ];
	$new_entry = $id;

	open $fh, ">>", "/home/iblech/radbotschaft/log.lst" or die $!;
	print $fh "$id\t$date\t$name\t$team\t$count\n";

	$changes++;
    }

    if(
	defined $name and defined $contact and defined $address and length($name) and $count > 0 and
	not ($name =~ /[\r\n]/ or $team =~ /[\r\n]/ or $contact =~ /[\r\n]/ or $address =~ /[\r\n]/) and
	length $name < 100 and length $team < 100 and length $count < 100 and length $contact < 100 and length $address < 100
    ) {
	open my $fh, ">>", "/home/iblech/radbotschaft/contacts.lst" or die $!;
	print $fh "$id\t$date\t$name\t$team\t$contact\t$address\n";
    }
}

my $sum = 0;
$sum += $ambassadors{$_}[2] for keys %ambassadors;

my $num_ambassadors = grep { $_->[2] > 0 } values %ambassadors;

sub max { $_[0] < $_[1] ? $_[1] : $_[0] }

push @output, <<TMPL;
<p><strong>Stand 29.1.</strong>, einem Tag nach der ersten Ankündigung dieser Webseite: <strong>26 Radlbotschafter*innen</strong> schätzen, dass sie insgesamt <strong>1250 Unterschriften</strong> sammeln werden. Wow!</p>
<!--<div>
  <progress max="30000" value="$sum" title="$sum von 30.000 via $num_ambassadors Radlbotschafter*innen"></progress>
</div>-->

<form action="index.pl" method="post">
  <table>
    <tr>
      <th class="highlight">Vorname/<br>Pseudonym/<br>Lieblingstier/<br>Lebensmotto</th>
      <th class="highlight">Team</th>
      <th class="highlight">Schätzung für Anzahl Unterschriften</th>
    </tr>
TMPL

for my $id (sort { $ambassadors{$b}[2] <=> $ambassadors{$a}[2] or $ambassadors{$a}[0] cmp $ambassadors{$b}[0] } keys %ambassadors) {
    my $a = $ambassadors{$id};
    next unless $a->[2] > 0;
    push @output, sprintf
	"    <tr id=\"row-$id\">" .
	"<td id=\"name-$id\">%s</td>" .
	"<td id=\"team-$id\">%s</td>" .
	"<td id=\"count-$id\" style=\"width: 3em; text-align: right\">%s</td>" .
	"<td onclick=\"edit('$id')\"><span id=\"edit-$id\">✎</span><button type=\"button\" id=\"submit-$id\" style=\"display: none\" onclick=\"send('$id')\">✓</button></td></tr>\n",
	escape_html($a->[0]), escape_html($a->[1]), $a->[2];
}

push @output, <<TMPL;
    <tr class="ambassador">
      <td><input type="text" name="name" placeholder="Dein Vorname/Pseudonym/Lieblingstier/Lebensmotto" size="15"></td>
      <td><input type="text" name="team" placeholder="Dein Teamname (optional)" size="10"></td>
      <td><input type="number" name="count" min="1" max="10000" step="1" value="10" size="3" style="width: 3em"></td>
      <td><button type="submit" onclick="
	if(document.getElementById('contact-yes').checked && document.getElementById('contact-address').value.length == 0) {
	  alert('Bitte gib noch deine Mail-Adresse oder Handynummer an, oder wähle eine andere Form der Kontaktmöglichkeit aus.');
	  return false;
	}
	return true;
      ">✓</button></td>
    </tr>
    <tr>
      <td colspan="4" style="font-size: smaller">
	Da die Unterschriftensammlung erst am 1. März beginnt, nach Abschluss
	der juristischen Prüfung,
	brauchen wir eine Möglichkeit, dich zu kontaktieren. Bitte wähle
	aus:
	<table>
	  <tr>
	    <td><input type="radio" name="contact" value="yes" id="contact-yes" checked="checked"></td>
	    <td>
	      <label for="contact-yes">
		Bitte erinnert mich per Mail oder WhatsApp/Signal/Telegram, wenn es los geht:
		<input id="contact-address" type="text" name="address" placeholder="Mail-Adresse oder Handynummer" onchange="document.getElementById('contact-yes').checked = true" size="30">
	      </label>
	    </td>
	  </tr>
	  <tr>
	    <td><input type="radio" name="contact" value="already" id="contact-already-whatsapp"></td>
	    <td>
	      <label for="contact-already-whatsapp">
		Ich gehe selbst in die
		<a href="https://chat.whatsapp.com/CSRw8xVsFFv4r14tcRiILM">WhatsApp-Gruppe</a>
		(oder bin schon in ihr drin).
	      </label>
	    </td>
	  </tr>
	  <tr>
	    <td><input type="radio" name="contact" value="already" id="contact-already-telegram"></td>
	    <td>
	      <label for="contact-already-telegram">
		Ich gehe selbst in die
		<a href="https://t.me/radentscheidaugsburg">Telegram-Gruppe</a>
		(oder bin schon in ihr drin).
	      </label>
	    </td>
	  </tr>
	  <tr>
	    <td><input type="radio" name="contact" value="already" id="contact-already-mail"></td>
	    <td>
	      <label for="contact-already-mail">
		Ich schreibe euch selbst eine <a
		href="mailto:radentscheid\@fff-augsburg.de">Mail an
		radentscheid\@fff-augsburg.de</a> (oder ich schrieb euch bereits).
	      </label>
	    </td>
	  </tr>
	  <tr>
	    <td><input type="radio" name="contact" value="no" id="contact-no"></td>
	    <td>
	      <label for="contact-no">
		Ich möchte nicht erinnert werden.
	      </label>
	    </td>
	  </tr>
	</table>
      </td>
    </tr>
  </table>
</form>

<p>Du möchtest mithelfen? Prima! Trag in die Tabelle eine <span
class="highlight">Schätzung</span> dafür ein, wie viele Unterschriften du
vermutlich sammeln können wirst: in der Familie, bei Freund*innen, auf der
Arbeit. Du kannst deine Schätzung später auch noch mal ändern. Diese Liste hier
dient nur für eine vorsichtige Vorabprognose sowie zur gegenseitigen
Motivation. 😃</p>
<p>Vielen Dank für deine Unterstützung. 💚</p>

<h2><q>Rettet die Bienen</q> fürs Rad</h2>

<h3>Was ist der Augsburger Radentscheid?</h3>

<p>Nach dem Vorbild Münchens und sieben weiteren Städten Bayerns ist der
Radentscheid zunächst ein Bürgerbegehren, das das Ziel hat, die Mobilitätswende
entscheidend voranzutreiben. Augsburg soll Fahrradstadt werden.</p>

<h3>Wie lauten unsere Forderungen?</h3>
<p>(Der genaue Wortlaut befindet sich noch in der juristischen Prüfung.)</p>
<ul>
<li>Durchgängiges Radwegenetz ohne abrupte Unterbrechungen, kein ständiger
Wechsel der Radwegeführung auf einer Strecke</li>
<li>Mehr Sicherheit an Kreuzungen. Vor allem Unfallschwerpunkte sind zu
entschärfen, zum Beispiel in dem die Sichtbarkeit der Radfahrer*innen
verbessert wird.</li>
<li>Beseitigung des derzeitigen Mangels an gut zugänglichen und sicheren
Radabstellmöglichkeiten. Orte des öffentlichen Lebens und ÖPNV-Knoten erhalten
neue Radabstellplätze.</li>
<li>Änderung der Stellplatzsatzung, sodass Bewohner*innen vor allem von
Mehrfamilienhäusern ihre Räder sicher und bequem unterbringen können.</li>
<li>Schaffung von mehr Platz für das Rad durch Umwidmung von jährlich 3 % der
öffentlichen Kraftfahrzeugstellplätze in der Innenstadt.</li>
<li>Verbesserung der Kommunikation zwischen Bürger*innen und Stadtverwaltung
durch eine Online-Meldeplattform, auf der Radfahrer*innen jederzeit störende
und gefährliche Wegstellen melden können. </li>
<li>Schaffung der personellen und finanziellen Voraussetzungen zur Umsetzung
des Bürgerentscheids in den städtischen Behörden.</li>
</ul>

<h3>Wer profitiert davon?</h3>
<p>Augsburg als Fahrradstadt ist nicht nur gut für Radfahrer*innen und Klima
(Straßenverkehr ist Deutschlands drittgrößter CO₂-Verursacher), sondern für
alle Augsburger*innen: Fußgänger*innen profitieren von weniger Lärm und
Abgasen, die Innenstadtbewohner*innen von mehr Lebensqualität und die ganze
Gesellschaft von weniger Unfällen.</p>

<h3>Wer organisiert das?</h3>
<p>Augsburgs Radentscheid ist ein gemeinsames Projekt von <a
href="https://forum-augsburg-lebenswert.de/">FAL (Forum Augsburg
lebenswert)</a>, <a href="http://www.adfc-augsburg.de/">ADFC Augsburg</a>,
<a href="https://www.fff-augsburg.de/">FFF Augsburg</a> und zahlreichen
Einzelpersonen. Unterstützung im Orgateam ist immer willkommen.</p>

<h3>Was sagt die Presse dazu?</h3>
<ul>
<li><a href="https://www.augsburger-allgemeine.de/augsburg/Rad-Aktivisten-planen-ein-Buergerbegehren-in-Augsburg-id56566326.html">Augsburger Allgemeine vom 27.1.</a></li>
<li><a href="https://www.augsburger-allgemeine.de/augsburg/Fahrradstadt-Augsburg-Wie-es-mit-dem-Begehren-weitergeht-id56577046.html">Augsburger Allgemeine vom 28.1.</a></li>
<li><a href="https://www.daz-augsburg.de/radentscheid-was-die-aktivisten-wollen/">DAZ vom 30.1.</a></li>
</ul>

<h3>Was wird das Bürgerbegehren bewirken?</h3>
<p>Sammeln wir etwa 13.000 Unterschriften, so hat der Stadtrat zwei Möglichkeiten:</p>
<ol>
<li>Entweder führt er einen Bürgerentscheid durch, bei dem dann (wie bei
"Rettet die Bienen") alle Wahlberechtigten im Rathaus abstimmen können, ob sie
für oder gegen die Forderungen sind. Bei einfacher Mehrheit ist der Stadtrat
dann ein Jahr an die Forderungen gebunden.</li>
<li>Oder der Stadtrat übernimmt ohne Bürgerentscheid direkt die Forderungen des
Begehrens. Das wird umso wahrscheinlicher, je mehr Unterschriften über die
13.000 hinaus wir präsentieren. Auch in diesem Fall ist der Stadtrat dann ein
Jahr an die Forderungen gebunden.</li>
</ol>

<h3>Welche Erfahrungen gibt es?</h3>
<p>In München wurde der Radentscheid zum erfolgreichsten Bürgerbegehren aller
Zeiten. 160.000 Menschen unterschrieben. Der Stadtrat nahm das Begehren ohne
weiteren Entscheid an. Seitdem fallen im Stadtrat im Wochentakt Entscheidungen,
für die sich Münchens Radcommunity zuvor jahrelang vergeblich einsetzte. Dieser
große Erfolg war nur dank mehr als 1.000 Radlbotschafter*innen möglich.</p>

<h3>Was sind Radlbotschafter*innen?</h3>
<p>Radlbotschafter*innen sind Menschen, die in ihrem Umfeld Unterschriften für
das Bürgerbegehren sammeln: in der Familie, im Freundeskreis oder auf der
Arbeit. Ein paar besonders Motivierte kündigten an, 100 oder mehr
Unterschriften sammeln zu wollen; die meisten sammeln eine große Handvoll. Wenn
Augsburgs Radentscheid ein Erfolg wird und Augsburg dadurch deutlich
lebenswerter wird, dann ist das allein den Radlbotschafter*innen zu verdanken.</p>

<h3>Wie geht es weiter?</h3>
<p>Die Unterschriftensammlung startet nach Abschluss der juristischen Prüfung
auf der FFF-Demo am 1. März (Sonntag, 15:00 Uhr, Rathausplatz). Unterschreiben
können dann alle volljährigen EU-Bürger*innen, die sich seit mindestens zwei
Monaten in Augsburg <q>mit dem Schwerpunkt ihrer Lebensbeziehungen
aufhalten</q>. In Kürze wird eine Webseite mit weiteren Informationen online gehen.
</p>

<h3>Was kann ich tun?</h3>
<ol>
<li>Sprich in deinem Freundeskreis über den Radentscheid!</li>
<li>Lade zwei oder drei Freund*innen in diesen Kanal ein!</li>
<li>Wenn du mit dem Gedanken spielst, Radlbotschafter*in zu werden: Dann trag
oben unverbindlich deine Schätzung dafür ein, wie viele Unterschriften du
vermutlich sammeln wirst. Das hilft für eine vorsichtige Vorabprognose und vor
allem zur gegenseitigen Motivation. 😊</li>
<li>Hab gute Laune, denn Augsburg wird mit großer Wahrscheinlichkeit bald viel schöner. 🎉</li>
</ol>

<p>Melde dich bei allen Fragen zum Radentscheid oder dieser Webseite bei Ingo:
<a
href="mailto:radentscheid\@fff-augsburg.de">radentscheid\@fff-augsburg.de</a>,
<a href="tel:+4917695110311">+49 176 95110311</a></p>
</body></html>
TMPL

print @output;

# Nicht Teil von @output, um nicht statisch gecached zu werden
if($new_entry) {
    print "<script>document.getElementById('row-$new_entry').className = 'updated';</script>\n";
}

if($changes) {
    open my $fh, ">", "index.html" or die $!;
    print $fh @output;
}
